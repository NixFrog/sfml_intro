solution "SFML-Intro"
	configurations ""
	buildoptions({"-std=c++11","-Wextra", "-O2", "-s"})

	
	project "sfml"
		kind "WindowedApp"
		language "C++"
		includedirs { "src" }
		files {
			"src/*.cpp", "src/*.hpp",
			"src/**/*.cpp", "src/**/*.hpp"
		}

		flags({"Symbols", "ExtraWarnings"})

		linkoptions({"-std=c++11", "-lsfml-graphics", "-lsfml-window", "-lsfml-system"})
