#pragma once

//Standard
#include <iostream>

//SFML
#include <SFML/Graphics.hpp>

//Own
#include "ResourceHolder.hpp"
#include "Textures.hpp"

class Game
{
public:
	Game();
	void run();

private:
	void processEvents();
	void update(sf::Time timeStep);
	void render();
	void handlePlayerInput(sf::Keyboard::Key key, bool isPressed);

private:
	sf::RenderWindow _Window;
	sf::Time _TimePerFrame;
	ResourceHolder<sf::Texture, Textures> _Textures;
	sf::Sprite _Player;

	bool _IsMovingUp;
	bool _IsMovingDown;
	bool _IsMovingLeft;
	bool _IsMovingRight;
};
