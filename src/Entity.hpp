#ifndef __ENTITY_HPP__
#define __ENTITY_HPP__

#include <SFML/System/Vector2.hpp>

class Entity
{
public:
	void setVelocity(sf::Vector2f velocity);
	void setVelocity(float vx, float vy);
	sf::Vector2f getVelocity() const;

private:
	sf::Vector2f m_Velocity;
};

#endif //__ENTITY_HPP__