#pragma once

#include <SFML/Graphics.hpp>

enum class Textures
{
	Background,
	Player,
	Missile
};