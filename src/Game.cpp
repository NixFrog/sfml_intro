#include "Game.hpp"

Game::Game():
	_Window(sf::VideoMode(640, 480), "SFML Window"),
	_Player()
{
	_Textures.load(Textures::Player, "resources/textures/suwako.png");

	_Player.setTexture(_Textures.get(Textures::Player));
	_Player.setPosition(100.0f, 100.0f);
	
	_TimePerFrame = sf::seconds(1.f / 60.f);
	_Window.setVerticalSyncEnabled(true);

}

void Game::run(){
	sf::Clock clock;
	sf::Time timeSinceLastUpdate = sf::Time::Zero;

	while(_Window.isOpen()){
		processEvents();
		timeSinceLastUpdate += clock.restart();
		while(timeSinceLastUpdate > _TimePerFrame){
			timeSinceLastUpdate -= _TimePerFrame;
			processEvents();
			update(_TimePerFrame);
		}
		render();
	}
}

void Game::processEvents(){
	sf::Event event;
	while(_Window.pollEvent(event)){
		switch(event.type){
			case sf::Event::KeyPressed:
				handlePlayerInput(event.key.code, true);
				break;
			case sf::Event::KeyReleased:
				handlePlayerInput(event.key.code, false);
				break;
			case sf::Event::Closed:
				_Window.close();
				break;
			default:
				break;
		}
		if(event.type == sf::Event::Closed){
			_Window.close();
		}
	}
}

void Game::update(sf::Time timeStep){
	sf::Vector2f movement(0.f, 0.f);
	if(_IsMovingUp){
		movement.y -= 10.f;
	}
	if(_IsMovingDown){
		movement.y += 10.f;
	}
	if(_IsMovingLeft){
		movement.x -= 10.f;
	}
	if(_IsMovingRight){
		movement.x += 10.f;
	}

	_Player.move(movement * timeStep.asSeconds());
}

void Game::render(){
	_Window.clear();
	_Window.draw(_Player);
	_Window.display();
}

void Game::handlePlayerInput(sf::Keyboard::Key key, bool isPressed){
	if(key == sf::Keyboard::Z){
		_IsMovingUp = isPressed;
	}
	else if(key == sf::Keyboard::S){
		_IsMovingDown = isPressed;
	}
	else if(key == sf::Keyboard::Q){
		_IsMovingLeft = isPressed;
	}
	else if(key == sf::Keyboard::D){
		_IsMovingRight = isPressed;
	}
}