#ifndef __CHARACTER_HPP__
#define __CHARACTER_HPP__

#include "Entity.hpp"

class Character : public Entity
{
public:
	enum class CharacterType
	{
		Suwako,
		Cirno,
	};

public:
	explicit Character(CharacterType type);

private:
	CharacterType _characterType;
};

#endif //__CHARACTER_HPP__