#ifndef __RESOURCE_HOLDER_HPP__
#define __RESOURCE_HOLDER_HPP__

//Standard
#include <string>
#include <cassert>

template <typename Resource, typename Identifier>
class ResourceHolder
{
public:
	/*
	* Generic loader for SFML resource types other than Shader and Music
	* @param id: Identifier refering to the resource
	* @param filename: name of the file where the resource is stored
	*/
	void load(Identifier id, const std::string& filename);

	/*
	* Generic loader for resource types requiring a second argument for loadFromFile()
	* @param id: Identifier refering to the resource
	* @param filename: name of the file where the resource is stored
	* @param secondParameter: caracterizes the resource type.
	*/
	template <typename Parameter>
	void load(Identifier id, const std::string&filename, const Parameter& secondParameter);

	Resource& get(Identifier id);
	const Resource& get(Identifier id) const;

private:
	std::map<Identifier, std::unique_ptr<Resource>> _ResourceMap;
};

#include "ResourceHolder.inl"

#endif //__RESOURCE_HOLDER_HPP__