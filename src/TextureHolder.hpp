#pragma once

//Standard
#include <map>
#include <iostream>
#include <cassert>

//Own
#include "Textures.hpp"

class TextureHolder
{
public:
	void load(Textures textureID, const std::string &filename);
	sf::Texture &get(Textures textureID);
	const sf::Texture &get(Textures textureID) const;

private:
	std::map<Textures, std::unique_ptr<sf::Texture>> _TextureMap;

};