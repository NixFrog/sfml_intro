#include "TextureHolder.hpp"

void TextureHolder::load(Textures textureID, const std::string &filename){
	std::unique_ptr<sf::Texture> texture(new sf::Texture());
	
	if(! texture->loadFromFile(filename)){
		throw std::runtime_error("TextureHolder::load - Failed to load " + filename);
	}

	auto inserted = _TextureMap.insert(std::make_pair(textureID, std::move(texture)));
	assert(inserted.second);
}

sf::Texture& TextureHolder::get(Textures textureID){
	auto mapTexture = _TextureMap.find(textureID);
	return *mapTexture->second;
}

const sf::Texture& TextureHolder::get(Textures textureID) const{
	auto mapTexture = _TextureMap.find(textureID);
	assert(mapTexture != _TextureMap.end());

	return *mapTexture->second;
}